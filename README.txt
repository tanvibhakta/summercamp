
This website was created by Libereco (gitlab.com/libereco) at the request of voidspace.xyz for use at a camp held in Trichy.

The original theme was taken from @ajkln.
All assets were created by Rahul Kondi.
Most code was written by Aditya Saky (saky.in).
Most content was written by Tanvi Bhakta (gitlab.com/tanvibhakta).
Lots of hair was pulled out by Voidspace (voidspace.xyz).


Everything in this repository is Free and Open to use under the CC BY-SA 3.0 license

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		Misc. Sass functions (@HugoGiraudel)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)
